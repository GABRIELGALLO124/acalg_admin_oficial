module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: process.env.NODE_ENV === 'production' ? '/ky-fetish-admin/' : '/',

  pluginOptions: {
    cordovaPath: 'src-cordova'
  }
}


